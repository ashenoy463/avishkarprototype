# Avishkar Prototype

Code, poster and slides for my entry to the Avishkar 2022 zonal round at St. Andrews.

- [Poster](presentation/poster.pdf)  
- [Slides](presentation/slides.pdf)

## Directory Structure

```
.
├── assets
│   └── <figures and diagrams>
├── presentation
│   └── <slides and poster>
├── <code>
└── README.md
```
